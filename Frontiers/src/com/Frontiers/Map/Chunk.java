package com.Frontiers.Map;

import com.Frontiers.Constants;
import com.Frontiers.Map.Tile.Tile;

public class Chunk {
	


	
	short x,y;
	Tile[] aTiles;
	
	public Chunk(MapGen oGen, short x, short y){
		aTiles = new Tile[Constants.CHUNK_SIZE * Constants.CHUNK_SIZE];
		this.x = x;
		this.y = y;
		
		int iXStart = x << Constants.CHUNK_SHIFT; // * CHUNK_SIZE
		int iYStart = y << Constants.CHUNK_SHIFT;
		
		for (int iY = 0; iY < Constants.CHUNK_SIZE; iY++){
			for (int iX = 0; iX < Constants.CHUNK_SIZE; iX++){
				aTiles[(iY << Constants.CHUNK_SHIFT) + iX] = oGen.generateTile(iX + iXStart, iY + iYStart);
			}
		}
		
	}
	
	
	public Tile getTile(int x, int y){
		//if (y < 0){ y += Constants.CHUNK_SIZE; }
		//if (x < 0){ x += Constants.CHUNK_SIZE; }
		return aTiles[(y << Constants.CHUNK_SHIFT) + x]; 	
	}	
}
