package com.Frontiers.Map;

import java.util.HashMap;

import com.Frontiers.Constants;
import com.Frontiers.Map.Tile.Tile;


/**
 * @author Matthew
 *
 */
public class Map {

	protected MapGen oGenerator;
	HashMap<Integer, Chunk> ChunkMap; 
	
	public Map(){
		ChunkMap = new HashMap<Integer, Chunk>();
		oGenerator = new MapGen(); 
	}
	
/**
 * Generates a chunk from coords 
 * @param x
 * @param y
 * @return
 */
	private Chunk generateChunk(short x, short y){
		return new Chunk(oGenerator,x,y);
	}
	
	/**
	 * fetches a chunk from disk
	 * @param key
	 * @return
	 */
	private Chunk loadChunk(int key) {
		// TODO Auto-generated method stub
		return null;
	}	
	
	/**
	 * fetches a chunk, caches it if 
	 * @param x
	 * @param y
	 * @return
	 */
	Chunk getChunk(short x, short y){
		int key = (((int) y)	<< 14) + x;
		Chunk oFindChunk = ChunkMap.get(key);
		if (oFindChunk == null){
			//try fetch from data store...
			oFindChunk = loadChunk(key);
			//build a new chunk
			if (oFindChunk == null){
				oFindChunk = generateChunk(x,y);
				ChunkMap.put(key, oFindChunk);
			}
		}
		return oFindChunk;
	}

	public Tile getTile(int x, int y){
		Chunk objChunk = getChunk((short)(x >>> Constants.CHUNK_SHIFT), (short) (y >>> Constants.CHUNK_SHIFT));
		return objChunk.getTile(x % Constants.CHUNK_SIZE, y % Constants.CHUNK_SIZE);
	}
	
	
	
	public void init(){

	}
	

}
