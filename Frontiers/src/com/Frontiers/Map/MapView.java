package com.Frontiers.Map;


import com.Frontiers.Constants;
import com.Frontiers.VectorInt;
import com.Frontiers.Gui.Console;
import com.Frontiers.Gui.Gadget;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;

public class MapView extends Gadget{

	OrthographicCamera oCam;
	Map oMap;
	Vector2 centreTile;
	Vector2 sizeTile;
	Vector2 screenCentre;
	VectorInt selectedTile;
	int xOffs, yOffs; // current x and y offset of the tile grid from the screen

	
	float zoom;
	int tileWidth, tileHeight;
	
	public MapView(Map map){
		oMap = map;
		zoom = 1f;
		this.sizeTile = new Vector2();
		this.centreTile = new Vector2();
		this.screenCentre = new Vector2();
	}
	
	public synchronized void setMapPosition(int x, int y){
		centreTile.set(x, y);
	}

	public void UpdateView(){
		if (iWidth > 0 && iHeight > 0){
			int viewWidth = Math.round((float) this.iWidth * zoom);
			int viewHeight = Math.round((float) this.iHeight * zoom);
			oCam = new OrthographicCamera(viewWidth, viewHeight);
			//set view so 0,0 is bottom left
			oCam.translate(viewWidth / 2, viewHeight / 2, 0 );
			oCam.update();	
			//get screen dimensions in tiles
			this.sizeTile.set((float) viewWidth / (float) Constants.TILE_SIZE, (float) viewHeight / (float) Constants.TILE_SIZE);
		}
	}
	
	
	public void setSize(int width, int height){
		super.setSize(width, height);
		this.screenCentre = new Vector2((float)width / 2, (float)height / 2);
		UpdateView();
	}

	public synchronized float getX() {
		return centreTile.x;
	}

	public synchronized float getY() {
		return centreTile.y;
	}


	
	public void ZoomIn(float amount){
		setZoom(getZoom() + amount);
	}
	
	
	public synchronized float getZoom() {
		return this.zoom;
	}

	
	
	public synchronized void setZoom(float zoom) {
		if (zoom > 3f ) zoom = 3f;
		if (zoom < 0.2f) zoom = 0.2f;
		this.zoom = zoom;
		UpdateView();
	}
	
	public synchronized void pan(int x, int y){
		centreTile.add(-((float) x / (float) Constants.TILE_SIZE) * this.zoom, -((float) y / (float) Constants.TILE_SIZE) * this.zoom);		
	}
	
	@Override
	protected void drawGadget(SpriteBatch oBatch, int xoffs, int yoffs){
		//backup matrix
		Matrix4 oOldMatrix = oBatch.getProjectionMatrix();
		oBatch.disableBlending();
		oBatch.setProjectionMatrix(oCam.combined);
		
		Vector2 startTile = new Vector2(this.centreTile.x - (this.sizeTile.x / 2f), this.centreTile.y - (this.sizeTile.y / 2f));
		int xStart = (int) Math.floor(startTile.x);
		int yStart = (int) Math.floor(startTile.y);		
		this.xOffs = (int) Math.floor((xStart - startTile.x) * (float) Constants.TILE_SIZE);
		this.yOffs = (int) Math.floor((yStart - startTile.y) * (float) Constants.TILE_SIZE);
		
		xoffs +=  this.xOffs;
		yoffs += this.yOffs;
		
		int width = (int) Math.ceil(this.sizeTile.x) + 1;
		int height = (int) Math.ceil(this.sizeTile.y) + 1;

		for(int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){			
				oMap.getTile(x + xStart, y + yStart).draw(oBatch,(x << Constants.TILE_SHIFT) + xoffs, (y << Constants.TILE_SHIFT ) + yoffs);
		
				//ResourceManager.getInstance().getFont("debug").draw(oBatch, String.format("%d,%d",x + xStart, y + yStart),(x << Constants.TILE_SHIFT) + this.xOffs,(y << Constants.TILE_SHIFT ) + this.yOffs);	
				//ResourceManager.getInstance().getFont("debug").draw(oBatch, String.format("%d",(int)Math.floor( 2f * ImprovedNoise.noise((double)(x+xStart) * 0.1f, (double)(y+yStart) * 0.1f, 1))),(x << Constants.TILE_SHIFT) + this.xOffs,(y << Constants.TILE_SHIFT ) + this.yOffs - 4);
				
			}
		}
		oBatch.enableBlending();
		//restore original matrix
		oBatch.setProjectionMatrix(oOldMatrix);
		
	}

	
	public void Select(int x, int y) {
		Vector2 point = new Vector2(x,y);
		point.sub(this.screenCentre).mul(1/(this.zoom * Constants.TILE_SIZE)).add(this.centreTile);

		this.selectedTile = new VectorInt(point);
		Console.getInstance().addLine("point: " + selectedTile.x + " , " + selectedTile.y + " selected perlin: " + (int)Math.floor( 3f * ImprovedNoise.noise((double)x * 0.125f, (double)y * 0.125f, 0)));
		
	}

	@Override
	public boolean click(int x, int y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

}
