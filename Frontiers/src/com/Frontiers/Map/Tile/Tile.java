package com.Frontiers.Map.Tile;

import com.Frontiers.Constants;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public abstract class Tile {

	protected TextureRegion oTR;
	public Tile(Texture oTexture, int x, int y){
		oTR = new TextureRegion(oTexture,x,y,Constants.TILE_SIZE,Constants.TILE_SIZE);
	}
	
	public void draw(SpriteBatch objBatch, int x, int y){
		objBatch.draw(oTR,x,y);
	}
	
	public abstract int getId();
	public abstract boolean isPassable();
	public abstract Tile tick();

	
}
