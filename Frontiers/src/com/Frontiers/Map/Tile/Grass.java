package com.Frontiers.Map.Tile;

import com.badlogic.gdx.graphics.Texture;
//import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Grass extends Tile {

	public Grass(Texture oTexture, int x, int y) {
		super(oTexture, x, y);
	}


	@Override
	public boolean isPassable() {
		return true;
	}

	@Override
	public Tile tick() {
		return this;
	}


	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return TileType.GRASS_TYPE;
	}

}
