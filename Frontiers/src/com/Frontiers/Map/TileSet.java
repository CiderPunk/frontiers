 package com.Frontiers.Map;

import java.util.Hashtable;

import com.Frontiers.ResourceManager;
import com.Frontiers.Map.Tile.Grass;
import com.Frontiers.Map.Tile.Tile;
import com.badlogic.gdx.graphics.Texture;

public class TileSet {

	protected Hashtable<Integer, Tile> tblIdTile;
	protected Hashtable<String, Tile> tblNameTile;
	
	
	private static TileSet _instance;
	
	private TileSet(){
		//tblIdTile = new Hashtable<Integer, Tile>();
		tblNameTile = new Hashtable<String,Tile>();
		
		Texture tex = ResourceManager.getInstance().getTexture("tiles");
		registerTile("grass", new Grass(tex,1,1));
		registerTile("mud", new Grass(tex,37,1));
		registerTile("rock", new Grass(tex,19,1));
		
	}
	
	/**
	 * singleton instance
	 * @return
	 */
	protected static TileSet getInstance(){
		if (_instance == null){
			_instance = new TileSet();
		}
		return _instance;
	}
	
	/**
	 * registers a tile type 
	 * @param name
	 * @param tile
	 */
	protected void registerTile(String name, Tile tile){
		tblNameTile.put(name, tile);
		//tblIdTile.put(tile.getId(), tile);
	}
	
	/**
	 * gets a tile type by name
	 * @param name
	 * @return
	 */
	public static Tile getTile(String name){
		return getInstance().tblNameTile.get(name);
	}
	
	/**
	 * gets a tile type by id
	 * @param id
	 * @return
	 */
	public static Tile getTile(int id){
		return getInstance().tblIdTile.get(id);
	}
	
}
