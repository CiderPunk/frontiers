package com.Frontiers.Gui;

import com.Frontiers.ResourceManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;


public abstract class TextGadget extends Gadget {

	public TextGadget (){
		super();
	}
	
	public TextGadget(int x, int y, int width, int height){
		super(x,y,width,height);
	}
	
	public TextGadget(float desiredWidth, float desiredHeight, ScreenPosition pos){
		super(desiredWidth, desiredHeight, pos);
	}
	
	private BitmapFont oFont;
	
	protected BitmapFont getFont(){
		if (oFont == null){
			oFont = ResourceManager.getInstance().getFont("smallfont");
		}
		return oFont;
	}
	
	
	
}
