package com.Frontiers.Gui;

import com.Frontiers.Gui.Patches.PatchManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TextButton extends TextGadget {

	public static final int PADDING = 8; 
	private String Text;
	
	public TextButton(String text){
		super();
		setText(text);
		
	}
	
	public String getText() {
		return Text;
	}

	public void setText(String text) {
		Text = text;
		TextBounds oBounds = this.getFont().getBounds(text);
		this.setSize((int) Math.ceil(oBounds.width) + (PADDING * 2), (int)Math.ceil(oBounds.height)  + (PADDING * 2));
	}

	@Override
	public boolean click(int x, int y) {
		if (x > this.iX && x < this.iX + this.iWidth && y > this.iY && y < this.iY + this.iHeight){
			
			return true;
		}
		return false;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}

	@Override
	public void draw(SpriteBatch batch, int x, int y) {
		PatchManager.getOutdentPatch().draw(batch, this.iX + x, this.iY + y, this.iWidth, this.iHeight);
		this.getFont().draw(batch, Text, this.iX + x + PADDING,  this.iY + y + PADDING + this.getFont().getLineHeight());
	}

}
