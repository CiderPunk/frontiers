package com.Frontiers.Gui;

import java.util.Iterator;
import java.util.LinkedList;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class Console extends TextGadget {
	
	public static final int MAX_MESSAGES = 100; 
	protected static Console _instance = null;
	
	
	protected int iMessageCount;
	protected int iLinesVisible; 


	protected LinkedList<DisplayMessage> lMessages;
	
	private Console(){
		super();
		lMessages = new LinkedList<DisplayMessage>();
		this.iMessageCount = 0;
		this.iLinesVisible = 0;
	}
	
	@Override
	void resize(int screenWidth, int screenHeight) {
		super.resize(screenWidth, screenHeight);
		this.iLinesVisible = (int) Math.floor((float) this.iHeight / this.getFont().getLineHeight());
		Iterator<DisplayMessage> it = this.lMessages.iterator();
		while (it.hasNext()){
			it.next().setSize(this.iWidth).update();
		}
	}

	/**
	 * gets singleton instance
	 * @return
	 */
	public static Console getInstance(){
		if (_instance == null){
			_instance = new Console();
		}
		return _instance;
	}
	
	
	/**
	 * adds a new line
	 * @param sValue
	 */
	public void addLine(String sValue){
		if (this.iMessageCount == MAX_MESSAGES){
			this.lMessages.removeLast();
		}
		//DisplayMessage msg =
		DisplayMessage msg = new DisplayMessage(sValue, this.getFont(), this.iWidth);
		this.lMessages.addFirst(msg);
	}


	@Override
	public int getMinHeight() {
		return 0;
	}

	@Override
	public int getMinWidth() {
		return 0;
	}

	@Override
	public boolean click(int x, int y) {
		return false;
	}

	@Override
	public void draw(SpriteBatch batch, int x, int y) {
		/*
		batch.end();

		
		if(ScissorStack.pushScissors(new Rectangle(this.iX, this.iY, this.iWidth, this.iHeight))){

			batch.begin();
			*/
			//GuiBack.getInstance().draw(batch, this.iX + x, this.iY + y, this.iWidth, this.iHeight);
			
			int linesToDraw = this.iLinesVisible;
			Iterator<DisplayMessage> it = this.lMessages.iterator();
			float fX =  (float) (this.iX + x );
			float fY = (float) (this.iY + y) + this.getFont().getLineHeight();
			
			
			while (linesToDraw > 0 && it.hasNext()){
				DisplayMessage message = it.next();
				if (message.getLines() < linesToDraw){
					linesToDraw-=message.getLines();
					message.draw(batch, fX, fY + (linesToDraw * this.getFont().getLineHeight()));
				}
				else{
					message.draw(batch, fX, fY, linesToDraw);
					linesToDraw-=message.getLines();
					break;
				}
			}
/*
			batch.end();

			ScissorStack.popScissors();

		}
batch.begin();
*/
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}


}
