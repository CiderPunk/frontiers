package com.Frontiers.Gui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class Gadget {
	///width and height of gadget
	protected int iWidth, iHeight;
	//x and y offset from bottom left
	protected int iX, iY;
	float fDesiredWidth, fDesiredHeight;
	public ScreenPosition iPos;

	Boolean bVisible;

	public Gadget (){
		this(0,0,0,0);
	}
	
	public Gadget(int x, int y, int width, int height){
		bVisible = true;
		setSize(width,height);
		setPosition(x,y);
		this.iPos = ScreenPosition.ABSOLUTE;

	}
	
	public Gadget(float desiredWidth, float desiredHeight, ScreenPosition pos){
		this();
		setPosition(desiredWidth, desiredHeight, pos);
	}
	
	
	public void setPosition(float desiredWidth, float desiredHeight, ScreenPosition pos)
	{
		this.iPos = pos;
		this.fDesiredHeight = desiredHeight;
		this.fDesiredWidth = desiredWidth;
	}
	/**
	 * resizes
	 * @param pad
	 * @param screenWidth
	 * @param screenHeight
	 * @return
	 */
	void resize(int screenWidth, int screenHeight){
		if (screenHeight > 0 && screenWidth > 0){
			setSize(fDesiredWidth > 0f ? (int) Math.ceil((float)screenWidth * fDesiredWidth) : iWidth,
					fDesiredHeight > 0f ? (int) Math.ceil((float)screenHeight * fDesiredHeight) : iHeight);
			switch(this.iPos){
				case TOP_LEFT:
					this.setPosition(0, screenHeight - iHeight);
					break;
				case TOP_RIGHT:
					this.setPosition(screenWidth - iWidth, screenHeight - iHeight);
					break;
				case BOTTOM_LEFT:
					this.setPosition(0,0);
					break;
				case BOTTOM_RIGHT:
					this.setPosition(screenWidth - iWidth ,0);
					break;
			}
		}
	}
	
	/**
	 * sets the dimensions for the gadget
	 * @param width
	 * @param height
	 * @return
	 */
	public void setSize(int width, int height){
		//get min width.height
		int minWidth = this.getMinWidth();
		int minHeight = this.getMinHeight();
		//set width/height
		iWidth = width > minWidth ? width: minWidth;
		iHeight = height > minHeight ? height : minHeight;

	}
		
	/**
	 * sets position relative to parent
	 * @param x
	 * @param y
	 * @return
	 */
	protected void setPosition(int x, int y){
		this.iX = x;
		this.iY = y;
	}
	
	public int getMinHeight() { 
		return 0; 
	}
	
	public int getMinWidth(){ 
		return 0; 
	}
	
	public abstract boolean click(int x, int y);
	public abstract void update();

	public void draw(SpriteBatch batch, int x, int y){
		drawGadget(batch, x,y);
	}
	
	protected abstract void drawGadget(SpriteBatch batch, int x, int y);

}
