package com.Frontiers.Gui.StatusItem;

import com.badlogic.gdx.Gdx;

public class StatusFPS implements IStatusItem {
	
	public StatusFPS(){}
	
	@Override
	public String getMessage() {
		return String.format("FPS: %d",Gdx.graphics.getFramesPerSecond());
	}

}
