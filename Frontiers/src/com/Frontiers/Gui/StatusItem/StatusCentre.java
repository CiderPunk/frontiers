package com.Frontiers.Gui.StatusItem;

import com.Frontiers.Map.MapView;

public class StatusCentre implements IStatusItem{

	protected MapView oView;
	
	public StatusCentre(MapView view){
		oView = view;
	}

	@Override
	public String getMessage() {
		return String.format("Centre: %.2f,%.2f", this.oView.getX(), this.oView.getY());
	}
	
	
}
