package com.Frontiers.Gui.StatusItem;

import com.Frontiers.Map.MapView;

public class StatusZoom implements IStatusItem {
	
	protected MapView oView;
	public StatusZoom(MapView view){
		oView = view;
	}

	@Override
	public String getMessage() {
		return  String.format("Zoom: %.2f", this.oView.getZoom());
	}

}
