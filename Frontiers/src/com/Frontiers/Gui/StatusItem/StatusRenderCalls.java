package com.Frontiers.Gui.StatusItem;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class StatusRenderCalls implements IStatusItem{

	protected SpriteBatch oBatch;
	
	public StatusRenderCalls(SpriteBatch batch){
		oBatch = batch;
	}

	@Override
	public String getMessage() {
		return String.format("R.Calls: %d, T.Calls: %d, B.Size: %d", this.oBatch.renderCalls, this.oBatch.totalRenderCalls, this.oBatch.maxSpritesInBatch);
	}
	
	
}
