package com.Frontiers.Gui;

public enum ScreenPosition {
	ABSOLUTE,
	TOP,
	LEFT,
	RIGHT,
	BOTTOM,
	TOP_LEFT,
	BOTTOM_LEFT,
	TOP_RIGHT,
	BOTTOM_RIGHT,
}
