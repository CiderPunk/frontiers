package com.Frontiers.Gui;

import java.util.Iterator;
import java.util.LinkedList;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class DisplayMessage {

	protected class StringSection{
		protected StringSection(int s, int e){
			start = s;
			end = e;
		}
		protected  int start;
		protected int end;
	}
	
	String sMessage;
	LinkedList<StringSection> lSections;
	BitmapFont oFont;
	int iLines;
	int iWidth;

	public DisplayMessage(BitmapFont font){
		oFont = font;
	}
	
	public DisplayMessage(String message, BitmapFont font, int width){
		this.sMessage = message.trim();
		this.oFont = font;
		this.setSize(width);
		if (this.iWidth > 0){
			this.update();
		}
	}
	
	public void draw(SpriteBatch batch, float x, float y, int lines){
		Iterator<StringSection> it = lSections.iterator();
		int iCount = 0;
		while (it.hasNext() && iCount < lines){
			StringSection section =  it.next();
			this.oFont.draw(batch, this.sMessage,x, y + ((iCount++) * this.oFont.getLineHeight()), section.start, section.end);
		}
	}
	
	public void draw(SpriteBatch batch, float x, float y){
		draw(batch, x, y, this.iLines);	
	}
	
	public int getLines(){
		return iLines;
	}
	
	public float getHeight(){
		return this.iLines * this.oFont.getLineHeight();
	}
	
	public DisplayMessage setMessage(String message){
		this.sMessage = message;
		return this;
	}

	public DisplayMessage setSize(int width){
		this.iWidth = width;
		return this;
	}
	
	public DisplayMessage update(){
		lSections = new LinkedList<StringSection>();
		int start = 0; 
		int end = 0;
 		int lineBreak = 0;
 		this.iLines = 0;

		while(lineBreak < this.sMessage.length()){
			this.iLines++;
			end = this.oFont.computeVisibleGlyphs(this.sMessage, start, this.sMessage.length(),(float) this.iWidth);
			lineBreak = start + end;

			if (lineBreak < this.sMessage.length()){
				for(int i = lineBreak; i > start; i-- ){
					if (sMessage.charAt(i) == ' '){
						lineBreak = i;
						break;
					}
				}
				
			}
			lSections.addFirst(new StringSection(start,lineBreak));
			
			start = lineBreak;
		}
		return this;
	}
	
	
	
}
