package com.Frontiers.Gui.Patches;

import com.Frontiers.ResourceManager;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class PatchManager {

	protected static  NinePatch _Border, _Outdent, _Indent;

	public static NinePatch getBorderPatch(){
		if (_Border == null){
			_Border= new NinePatch(new TextureRegion(ResourceManager.getInstance().getTexture("gui"), 0,92,24,24),8,8,8,8);;	
		}
		return _Border;
	}
	
	public static NinePatch getIndentPatch(){
		if (_Indent == null){
			_Indent= new NinePatch(new TextureRegion(ResourceManager.getInstance().getTexture("gui"), 25,117,24,24),8,8,8,8);;	
		}
		return _Indent;
	}
	
	public static NinePatch getOutdentPatch(){
		if (_Outdent == null){
			_Outdent= new NinePatch(new TextureRegion(ResourceManager.getInstance().getTexture("gui"), 0,117,24,24),8,8,8,8);;	
		}
		return _Outdent;
	}
	
	
	
	
	
}
