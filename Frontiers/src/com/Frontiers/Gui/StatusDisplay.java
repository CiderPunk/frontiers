package com.Frontiers.Gui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.Frontiers.Gui.StatusItem.*;
import com.Frontiers.Map.Map;
import com.Frontiers.Map.MapView;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class StatusDisplay extends TextGadget {
	List<IStatusItem> lstItems;
	final protected int WIDTH = 200;
	
	public StatusDisplay(MapView mapView, Map map, SpriteBatch batch){
		super(0.0f, 0.0f, ScreenPosition.TOP_LEFT);
		lstItems = new ArrayList<IStatusItem>();
		lstItems.add(new StatusFPS());
		lstItems.add(new StatusCentre(mapView));
		lstItems.add(new StatusZoom(mapView));
		lstItems.add(new StatusRenderCalls(batch));
	}

	
	@Override
	public void update() {
		// TODO Auto-generated method stub
	}

	@Override
	public int getMinHeight() {
		if (lstItems != null){
			return (int) Math.ceil(this.lstItems.size() * this.getFont().getLineHeight());
		}
		return 0;
	}

	@Override
	public int getMinWidth() {
		return WIDTH;
	}



	@Override
	public boolean click(int x, int y) {
		return false;
	}

	@Override
	public void draw(SpriteBatch batch, int x, int y) {
		Iterator<IStatusItem> itr = lstItems.iterator();
		int iCount = 1;
		while(itr.hasNext()){
			this.getFont().draw(batch, itr.next().getMessage(), x + this.iX, y + this.iY + (iCount * this.getFont().getLineHeight()));
			iCount++;
		}
	}
	
	
}
