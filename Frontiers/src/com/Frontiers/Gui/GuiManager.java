package com.Frontiers.Gui;

import java.util.LinkedList;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GuiManager extends ContainerGadget {

	protected OrthographicCamera oCam;
	
	public GuiManager(){
		super(1.0f, 1.0f, ScreenPosition.ABSOLUTE);
		this.setPadding(10);
		this.children = new LinkedList<Gadget>();
	}

	@Override
	public void resize(int screenWidth, int screenHeight){
		super.resize(screenWidth, screenHeight);
		oCam = new OrthographicCamera(screenWidth, screenHeight);
		oCam.translate(screenWidth / 2, screenHeight / 2, 0 );
		oCam.update();
	}
	
	@Override
	public void draw(SpriteBatch batch, int x, int y) {
	  batch.setProjectionMatrix(oCam.combined);
		super.draw(batch, x, y);
	}

}
