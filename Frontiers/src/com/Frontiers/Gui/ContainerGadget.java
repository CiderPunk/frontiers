package com.Frontiers.Gui;

import java.util.Iterator;
import java.util.LinkedList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class ContainerGadget extends Gadget {
	
	int iPadding;
	LinkedList<Gadget> children;
	
	public ContainerGadget(){
		super();
		children = new LinkedList<Gadget>();
	}
	
	public ContainerGadget(float desiredWidth, float desiredHeight, ScreenPosition pos){
		this();
		setPosition(desiredWidth, desiredHeight,  pos);
	}
	
	@Override
	void resize(int screenWidth, int screenHeight) {
		super.resize(screenWidth, screenHeight);
		assert(this.children!= null);
		Iterator<Gadget> it = this.children.iterator();
		while (it.hasNext()){
			it.next().resize(this.iWidth - (2 * this.iPadding), this.iHeight - (2 * this.iPadding));
		}
	}

	@Override
	public boolean click(int x, int y) {
		if (x > this.iX && x < this.iX + this.iWidth && y > this.iY && y < this.iY + this.iHeight){
			Iterator<Gadget> it = this.children.descendingIterator();
			while (it.hasNext()){
				if (it.next().click(x - this.iX - this.iPadding, y - this.iY - this.iPadding)){
					return true;
				}
			}	
		}
		return false;
	}

	@Override
	public void draw(SpriteBatch batch, int x, int y) {
		drawGadget(batch,x,y);
		drawChildren(batch,x,y);
	}

	protected void drawChildren(SpriteBatch batch, int x, int y){
		Iterator<Gadget> it = this.children.iterator();
		while (it.hasNext()){
			it.next().draw(batch, x + this.iX + iPadding, y + this.iY + iPadding);
		}
	}	
	
	public int getPadding() {
		return iPadding;
	}

	public void setPadding(int padding) {
		this.iPadding = padding;
	}

	public void update(){
		Iterator<Gadget> it = this.children.iterator();
		while (it.hasNext()){
			it.next().update();
		}
	}
	
	public void registerChild(Gadget childGadget){
		this.children.add(childGadget);
	}
	
	
}
