package com.Frontiers.Input;

import com.Frontiers.Gui.GuiManager;
import com.Frontiers.Map.MapView;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;

public class FrontiersGestureListener implements GestureListener{

	protected MapView oView;
	protected GuiManager oGui;
	float origZoom;
	
	
	public FrontiersGestureListener(MapView view, GuiManager gui){
		oView = view;
		origZoom = oView.getZoom();
		oGui = gui;
	}

/*
	public boolean touchUp(int x, int y, int pointer, int button){
		
		return false;
	}
	*/
	
	@Override
	public boolean fling(float arg0, float arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(int x, int y, int dx, int dy) {
		oView.pan(dx, -dy);
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialFirstPointer, Vector2 initialSecondPointer, Vector2 firstPointer, Vector2 secondPointer) {
		
		
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(int x, int y, int count) {
		if (!oGui.click(x, y)){
		//Console.getInstance().addLine(String.format("tap %d,%d,%d",  x,y,count));
			oView.Select(x,y);
		}
	
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int arg0, int arg1, int arg2) {
		origZoom = oView.getZoom();
		return false;
	}

	@Override
	public boolean zoom(float origDist, float newDist) {
		float ratio = origDist / newDist;
		oView.setZoom(origZoom * ratio );
		
		return true;
	}
	
}
