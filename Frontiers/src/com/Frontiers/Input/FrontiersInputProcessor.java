package com.Frontiers.Input;

import com.Frontiers.Gui.GuiManager;
import com.Frontiers.Map.MapView;
import com.badlogic.gdx.InputProcessor;

public class FrontiersInputProcessor implements InputProcessor{

	protected MapView oView;
	protected GuiManager oGui;
	
	public FrontiersInputProcessor(MapView view, GuiManager gui){
		oView = view;
		oGui = gui;
	}
	
	
	@Override
	public boolean keyDown(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int scrollAmount) {
		oView.ZoomIn((float)scrollAmount * 0.1f);
		return false;
	}

	@Override
	public boolean touchDown(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchMoved(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		return false;
	}

}
