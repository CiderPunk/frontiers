package com.Frontiers;

import java.util.HashMap;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class ResourceManager {

	protected static ResourceManager _instance = null;
	
	protected HashMap<String, BitmapFont> aFonts;
	protected HashMap<String, Texture> aTextures;
	

	private ResourceManager(){
		aFonts = new 	HashMap<String, BitmapFont>();
		aTextures = new HashMap<String, Texture>();
		BitmapFont fntSmallDebug = new BitmapFont();
		fntSmallDebug.setScale(0.4f);
		aFonts.put("debug", fntSmallDebug);

		aTextures.put("gui", loadTexture("res/gui.png",false));
		aTextures.put("tiles", loadTexture("res/tiles2.png",true));		
		
		aFonts.put("smallfont", new BitmapFont(Gdx.files.internal("res/arial14.fnt"), new TextureRegion( this.getTexture("gui"),0,0,256,256) , false));

	}
	
	private Texture loadTexture(String strFile, Boolean bMip){
		Texture oTex = new Texture(Gdx.files.internal(strFile),bMip);
		if (bMip){
			oTex.setFilter(TextureFilter.MipMap    , TextureFilter.MipMap    );
		}
		return oTex;
	}
	
	
	/**
	 * gets singleton instance
	 * @return
	 */
	public static ResourceManager getInstance(){
		if (_instance == null){
			_instance = new ResourceManager();
		}
		return _instance;
	}
	
	/**
	 * gets the names font
	 * @param key
	 * @return
	 */
	public BitmapFont getFont(String key){
		return aFonts.get(key);
	}
	/**
	 * gets the named texture
	 * @param key
	 * @return
	 */
	public Texture getTexture(String key){
		return aTextures.get(key);
	}
	
	
}
