package com.Frontiers;

import com.badlogic.gdx.math.Vector2;

public class VectorInt {

	public int x,y;

	public VectorInt(){
		x = y = 0;
	}
	
	public VectorInt(int iX, int iY){
		set (x,y);
	}
	public VectorInt(VectorInt v){
		set(v);
	}

	public VectorInt(Vector2 v){
		set(v);
	}
	
	public VectorInt set(int iX, int iY){
		x = iX;
		y = iY;
		return this;
	}
	
	public VectorInt set(VectorInt v){
		x= v.x;
		y= v.y;	
		return this;
	}
	
	public VectorInt set(Vector2 v){
		x= (int) Math.floor(v.x);
		y= (int) Math.floor(v.y);		
		return this;
	}
	
	
	public VectorInt add(VectorInt v){
		this.x += v.x;
		this.y += v.y;
		return this;
	}
	
	public VectorInt sub(VectorInt v){
		this.x -= v.x;
		this.y -= v.y;
		return this;
	}
	
	public int len2(){
		return x * x + y * y;
	}
	
	public float len(){
		return (float) Math.sqrt(len2());
	}
	
	public VectorInt shiftUp(int bits){
		this.x = this.x << bits;
		this.y = this.y << bits;
		return this;
	}
	
	public VectorInt shiftDown(int bits){
		this.x = this.x << bits;
		this.y = this.y << bits;
		return this;
	}
	
	
	
	
}
