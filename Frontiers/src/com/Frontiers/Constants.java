package com.Frontiers;

public class Constants {


	public static final int CHUNK_SHIFT =4;
	public static final int CHUNK_SIZE = 1<<CHUNK_SHIFT;
	public static final int TILE_SIZE = 16;
	public static final int TILE_SHIFT = 4;
	
}
