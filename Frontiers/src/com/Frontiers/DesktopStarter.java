package com.Frontiers;
import com.badlogic.gdx.backends.jogl.JoglApplication;

public class DesktopStarter {
	public static void main(String[] args){
		new JoglApplication( new Frontiers(),
				"Frontiers",
				640,480, false);
	}

}
