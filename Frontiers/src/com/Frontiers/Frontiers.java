package com.Frontiers;

import com.Frontiers.Gui.Console;
import com.Frontiers.Gui.GuiManager;
import com.Frontiers.Gui.ScreenPosition;
import com.Frontiers.Gui.StatusDisplay;
import com.Frontiers.Gui.TextButton;
import com.Frontiers.Input.FrontiersGestureListener;
import com.Frontiers.Input.FrontiersInputProcessor;
import com.Frontiers.Map.Map;
import com.Frontiers.Map.MapView;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;


public class Frontiers implements ApplicationListener {

	SpriteBatch oBatch;
	
	protected Map oMap;
	protected MapView oMapView;
	protected FrontiersGestureListener oGestureListener;
	protected FrontiersInputProcessor oInputProc;
	protected GestureDetector oGestureDetector;
	protected InputMultiplexer oInputMPlexer; 
	protected GuiManager oGuiManager;
	
	
	@Override
	public void create() {
		oBatch = new SpriteBatch();
		oBatch.maxSpritesInBatch = 500;
		oMap = new Map();
		oMap.init();
		
		oGuiManager = new GuiManager();		
		
		
		oMapView = new MapView(oMap);
		oMapView.setMapPosition(1 << 18, 1 << 18);
		
		
		oGuiManager.registerChild(oMapView);
		
		
		oGestureListener = new FrontiersGestureListener(oMapView, oGuiManager);
		oGestureDetector = new GestureDetector(20,0.5f,2,0.15f, oGestureListener);
		oInputProc = new FrontiersInputProcessor(oMapView, oGuiManager);
		oInputMPlexer= new InputMultiplexer(oGestureDetector, oInputProc);
		

		Console.getInstance().setPosition(1.0f, 0.2f, ScreenPosition.BOTTOM_LEFT);
		
	
		oGuiManager.registerChild(Console.getInstance());
		oGuiManager.registerChild(new StatusDisplay(oMapView, oMap, oBatch));
		
		TextButton oButton = new TextButton("douchebag");
		oButton.setPosition(0, 0, ScreenPosition.TOP_RIGHT);
		
		oGuiManager.registerChild(oButton);
		
		Gdx.input.setInputProcessor(oInputMPlexer);
		

		Console.getInstance().addLine("The quick brown fox jumped over the lazy dog and stuff like that yeah!!!! The quick brown fox jumped over the lazy dog and stuff like that yeah!!!!!");
		Console.getInstance().addLine("Katy rocks!Katy rocks!Katy rocks!Katy rocks!Katy rocks! Katy rocks!Katy rocks! Katy rocks! Katy rocks! Katy rocks!Katy rocks! Katy rocks!Katy rocks! Katy rocks!Katy rocks!");
		Console.getInstance().addLine("a1a2a3a4a5a6a7a8a9a0b1b2b3b4b5b6b7b8b9b0c1c2c3c4c5c6c7c8c9c0");
		
		
/*
		Console.getInstance().addLine("1");
		Console.getInstance().addLine("2");
		Console.getInstance().addLine("3");
		Console.getInstance().addLine("4");
		Console.getInstance().addLine("5");
		Console.getInstance().addLine("6");
		Console.getInstance().addLine("7");
		Console.getInstance().addLine("8");
		Console.getInstance().addLine("9");
*/
	}

	@Override
	public void dispose() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void render() {
		
    Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		this.oBatch.begin();
		oGuiManager.draw(this.oBatch,0,0);
		this.oBatch.end();
	}

	@Override
	public void resize(int width, int height) {
		//oMapView.setSize(width, height);
		oGuiManager.resize(width, height);
		
		/*
		Gdx.gl.glEnable(GL10.GL_SCISSOR_TEST);
		ScissorStack.pushScissors(new Rectangle(0,0, width,height));
		*/
	}

	@Override
	public void resume() {
	}

}
